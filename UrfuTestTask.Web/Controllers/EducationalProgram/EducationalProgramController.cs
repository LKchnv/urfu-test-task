﻿using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using UrfuTestTask.Core.Attributes;
using UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos;
using UrfuTestTask.UseCases.EducationalPrograms.Commands;
using UrfuTestTask.UseCases.EducationalPrograms.Queries;

namespace UrfuTestTask.Web.Controllers.EducationalProgram
{
    [Route("/api/[controller]")]
    [ApiController]
    public class EducationalProgramController : AbstractController
    {
        public EducationalProgramController(IMediator mediator) : base(mediator)
        { }

        [HttpGet("")]
        [BasicAuthorization]
        public async Task<IActionResult> GetAllEdPrograms(int limit = 10, int offset = 0)
        {
            var result = await mediator.Send(new GetAllEdProgramsQuery { Limit = limit, Offset = offset });
            return Ok(result);
        }

        [HttpGet("references")]
        [BasicAuthorization]
        public async Task<IActionResult> GetEdProgramReferences()
        {
            var result = await mediator.Send(new GetEdProgramsReferencesQuery());
            return Ok(result);
        }

        [HttpGet("{edProgramId:Guid}")]
        [BasicAuthorization]
        public async Task<IActionResult> GetEdProgram([FromRoute] Guid edProgramId)
        {
            var result = await mediator.Send(new GetEdProgramByIdQuery { Uuid = edProgramId });
            return Ok(result);
        }

        [HttpPatch("{edProgramId:Guid}")]
        [BasicAuthorization]
        public async Task<IActionResult> EditEdProgram
            ([FromRoute] Guid edProgramId,
            [FromBody] JsonPatchDocument<EducationalProgramRequestDto> patchDocument)
        {
            await mediator.Send(new EditEdProgramCommand { Uuid = edProgramId, PatchModel = patchDocument });
            return Ok();
        }

        [HttpPost("")]
        [BasicAuthorization]
        public async Task<IActionResult> AddEdProgram([FromBody] EducationalProgramRequestDto dto)
        {
            await mediator.Send(new AddEdProgramCommand { Program = dto });
            return Ok();
        }

        [HttpDelete("{edProgramId:Guid}")]
        [BasicAuthorization]
        public async Task<IActionResult> DeleteEdProgram([FromRoute] Guid edProgramId)
        {
            await mediator.Send(new DeleteEdProgramCommand { Uuid = edProgramId });
            return Ok();
        }
    }
}
