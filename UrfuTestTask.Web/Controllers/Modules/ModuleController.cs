﻿using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using UrfuTestTask.Core.Attributes;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;
using UrfuTestTask.UseCases.Modules.Commands;
using UrfuTestTask.UseCases.Modules.Queries;

namespace UrfuTestTask.Web.Controllers.Modules
{
    [Route("/api/[controller]")]
    [ApiController]
    public class ModuleController : AbstractController
    {
        public ModuleController(IMediator mediator) : base(mediator)
        { }

        [HttpGet("")]
        [BasicAuthorization]
        public async Task<IActionResult> GetAllModules(int limit = 10, int offset = 0)
        {
            var result = await mediator.Send(new GetAllModulesQuery { Limit = limit, Offset = offset });
            return Ok(result);
        }

        [HttpGet("{moduleId:Guid}")]
        [BasicAuthorization]
        public async Task<IActionResult> GetModule([FromRoute] Guid moduleId)
        {
            var result = await mediator.Send(new GetModuleByIdQuery { Uuid = moduleId });
            return Ok(result);
        }

        [HttpPatch("{moduleId:Guid}")]
        [BasicAuthorization]
        public async Task<IActionResult> EditModule
            ([FromRoute] Guid moduleId,
            [FromBody] JsonPatchDocument<ModuleRequestDto> patchDocument)
        {
            await mediator.Send(new EditModuleCommand { Uuid = moduleId, PatchModel = patchDocument });
            return Ok();
        }

        [HttpPost("")]
        [BasicAuthorization]
        public async Task<IActionResult> AddModule([FromBody] ModuleRequestDto module)
        {
            await mediator.Send(new AddModuleCommand { ModuleToAdd = module });
            return Ok();
        }

        [HttpDelete("{moduleId:Guid}")]
        [BasicAuthorization]
        public async Task<IActionResult> DeleteModule([FromRoute] Guid moduleId)
        {
            await mediator.Send(new DeleteModuleCommand { Uuid = moduleId });
            return Ok();
        }
    }
}
