﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace UrfuTestTask.Web.Controllers
{
    public class AbstractController : ControllerBase
    {
        protected readonly IMediator mediator;

        public AbstractController(IMediator mediator)
        {
            this.mediator = mediator;
        }
    }
}
