using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Net;
using System.Text.Json.Serialization;
using UrfuTestTask.Core.Authentication;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.Dtos;
using UrfuTestTask.Core.Models.MappingProfiles;
using UrfuTestTask.Core.Utils;
using UrfuTestTask.Infrastructure;
using UrfuTestTask.UseCases;
using UrfuTestTask.Web.JsonConverters;

namespace UrfuTestTask.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            var connStr = Environment.GetEnvironmentVariable("__URFU_TEST_CONN_STR__");

            if (string.IsNullOrEmpty(connStr))
            {
                connStr = builder.Configuration.GetConnectionString("UrfuTestDb");
            }

            builder.Services.AddControllers()
                .AddNewtonsoftJson();

            // Add services to the container.

            builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<AbstractHandler>());
            builder.Services.AddAutoMapper(typeof(AutomapperPing));

            builder.Services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.Converters.Add(new DateOnlyJsonConverter());
                });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Urfu Test Task", Version = "v1" });

                options.AddSecurityDefinition("Basic",
                    new Microsoft.OpenApi.Models.OpenApiSecurityScheme()
                    {
                        Name = "Authorization",
                        Type = Microsoft.OpenApi.Models.SecuritySchemeType.Http,
                        Scheme = "Basic",
                        In = Microsoft.OpenApi.Models.ParameterLocation.Header,
                        Description = "Basic Authentication Header"
                    });

                options.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Basic"
                            }
                        },
                        new string[] {"Basic "}
                    }
                });
            });

            builder.Services.AddAuthentication()
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>(
                    "Basic", null
                );

            builder.Services.AddDbContext<UrfuProgramsContext>(
                options => {
                    options
                    .UseNpgsql(connStr);
                });

            builder.Services.AddScoped<IUrfuProgramsContext>(provider =>
                provider.GetService<UrfuProgramsContext>());

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseExceptionHandler(o =>
            {
                o.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";

                    var ex = context.Features.Get<IExceptionHandlerFeature>();
                    if (ex != null)
                    {
                        var errorMessage = "An unexpected error occurred. Please try again later.";

                        if (ex.Error is NotFoundException)
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                            errorMessage = ex.Error.Message;
                        }

                        var errorResponse = new ExceptionResponseDto { ExceptionMessage = errorMessage };
                        await context.Response.WriteAsync(errorResponse.ToString());
                    }
                });
            });

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
