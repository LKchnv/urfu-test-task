﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UrfuTestTask.Infrastructure.Migrations
{
    public partial class CreatedModulesByEdProgsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "modules_by_educational_programs",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    edicational_program_uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    module_uuid = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_modules_by_educational_programs", x => x.Id);
                    table.ForeignKey(
                        name: "fk_EducationalProgramUuid",
                        column: x => x.edicational_program_uuid,
                        principalTable: "educational_program",
                        principalColumn: "uuid");
                    table.ForeignKey(
                        name: "fk_ModuleUuid",
                        column: x => x.module_uuid,
                        principalTable: "module",
                        principalColumn: "uuid");
                });

            migrationBuilder.CreateIndex(
                name: "IX_modules_by_educational_programs_edicational_program_uuid",
                table: "modules_by_educational_programs",
                column: "edicational_program_uuid");

            migrationBuilder.CreateIndex(
                name: "IX_modules_by_educational_programs_module_uuid",
                table: "modules_by_educational_programs",
                column: "module_uuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "modules_by_educational_programs");
        }
    }
}
