﻿using Microsoft.EntityFrameworkCore;
using Npgsql;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.DbModels;
using UrfuTestTask.Core.Models.DbModels.Enums;
using EdModule = UrfuTestTask.Core.Models.DbModels.Module;

namespace UrfuTestTask.Infrastructure
{
    public partial class UrfuProgramsContext : DbContext, IUrfuProgramsContext
    {
        public UrfuProgramsContext()
        {
        }

        static UrfuProgramsContext()
        {
            NpgsqlConnection.GlobalTypeMapper.MapEnum<EducationLevelEnum>("education_level");
            NpgsqlConnection.GlobalTypeMapper.MapEnum<EducationStandartEnum>("education_standart");
        }

        public UrfuProgramsContext(DbContextOptions<UrfuProgramsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<EducationalProgram> EducationalPrograms { get; set; } = null!;
        public virtual DbSet<Institute> Institutes { get; set; } = null!;
        public virtual DbSet<EdModule> Modules { get; set; } = null!;
        public virtual DbSet<ProgramHead> ProgramHeads { get; set; } = null!;
        public virtual DbSet<ModulesByEducationalProgram> ModulesByEducationalPrograms { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            { }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresEnum<EducationLevelEnum>("education_level")
                .HasPostgresEnum<EducationStandartEnum>("education_standart");

            modelBuilder.Entity<EducationalProgram>(entity =>
            {
                entity.HasKey(e => e.Uuid)
                    .HasName("educational_program_pkey");

                entity.ToTable("educational_program");

                entity.Property(e => e.Uuid)
                    .ValueGeneratedNever()
                    .HasColumnName("uuid");

                entity.Property(e => e.AccreditationTime).HasColumnName("accreditation_time");

                entity.Property(e => e.Cypher)
                    .HasMaxLength(30)
                    .HasColumnName("cypher");

                entity.Property(e => e.Head).HasColumnName("head");

                entity.Property(e => e.Institute).HasColumnName("institute");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Level).HasColumnName("level");
                
                entity.Property(e => e.Standart).HasColumnName("standart");

                entity.Property(e => e.Title).HasColumnName("title");

                entity.HasOne(d => d.HeadNavigation)
                    .WithMany(p => p.EducationalPrograms)
                    .HasForeignKey(d => d.Head)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("educational_program_head_fkey");

                entity.HasOne(d => d.InstituteNavigation)
                    .WithMany(p => p.EducationalPrograms)
                    .HasForeignKey(d => d.Institute)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("educational_program_institute_fkey");
            });

            modelBuilder.Entity<Institute>(entity =>
            {
                entity.HasKey(e => e.Uuid)
                    .HasName("institutes_pkey");

                entity.ToTable("institutes");

                entity.Property(e => e.Uuid)
                    .ValueGeneratedNever()
                    .HasColumnName("uuid");

                entity.Property(e => e.Title).HasColumnName("title");
            });

            modelBuilder.Entity<EdModule>(entity =>
            {
                entity.HasKey(e => e.Uuid)
                    .HasName("module_pkey");

                entity.ToTable("module");

                entity.Property(e => e.Uuid)
                    .ValueGeneratedNever()
                    .HasColumnName("uuid");

                entity.Property(e => e.Title).HasColumnName("title");

                entity.Property(e => e.Type).HasColumnName("type");
            });

            modelBuilder.Entity<ProgramHead>(entity =>
            {
                entity.HasKey(e => e.Uuid)
                    .HasName("program_heads_pkey");

                entity.ToTable("program_heads");

                entity.Property(e => e.Uuid)
                    .ValueGeneratedNever()
                    .HasColumnName("uuid");

                entity.Property(e => e.Fullname).HasColumnName("fullname");
            });

            modelBuilder.Entity<ModulesByEducationalProgram>(entity =>
            {
                entity.HasAlternateKey(x => x.Id);

                entity.ToTable("modules_by_educational_programs");

                entity.Property(e => e.EducationalProgramUuid)
                    .ValueGeneratedNever()
                    .HasColumnName("edicational_program_uuid");

                entity.Property(e => e.ModuleUuid)
                    .ValueGeneratedNever()
                    .HasColumnName("module_uuid");

                entity.HasOne(d => d.EducationalProgram)
                    .WithMany(p => p.ModulesByEducationalProgram)
                    .HasForeignKey(d => d.EducationalProgramUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_EducationalProgramUuid");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.ModulesByEducationalProgram)
                    .HasForeignKey(d => d.ModuleUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ModuleUuid");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
