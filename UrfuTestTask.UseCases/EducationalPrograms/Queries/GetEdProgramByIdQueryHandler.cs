﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos;

namespace UrfuTestTask.UseCases.EducationalPrograms.Queries
{
    public class GetEdProgramByIdQuery : IRequest<EducationalProgramResponseDto>
    {
        public Guid Uuid { get; set; }
    }

    public class GetEdProgramByIdQueryHandler
        : AbstractHandler, IRequestHandler<GetEdProgramByIdQuery, EducationalProgramResponseDto>
    {
        public GetEdProgramByIdQueryHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task<EducationalProgramResponseDto> Handle(GetEdProgramByIdQuery request, CancellationToken cancellationToken)
        {
            var edPro = await context.EducationalPrograms
                .Include(e => e.InstituteNavigation)
                .Include(e => e.HeadNavigation)
                .Include(e => e.ModulesByEducationalProgram)
                .ThenInclude(m => m.Module)
                .AsNoTracking()
                .SingleOrDefaultAsync(e => e.Uuid == request.Uuid);

            if (edPro == null)
            {
                throw new NotFoundException($"Образовательной программы с uuid {request.Uuid} не существует");
            }

            return mapper.Map<EducationalProgramResponseDto>(edPro);
        }
    }
}
