﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos;

namespace UrfuTestTask.UseCases.EducationalPrograms.Queries
{
    public class GetAllEdProgramsQuery : IRequest<List<EducationalProgramSummaryResponseDto>>
    {
        public int Limit { get; set; }
        public int Offset { get; set; }
    }

    public class GetAllEdProgramsQueryHandler
        : AbstractHandler, IRequestHandler<GetAllEdProgramsQuery, List<EducationalProgramSummaryResponseDto>>
    {
        public GetAllEdProgramsQueryHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task<List<EducationalProgramSummaryResponseDto>> Handle(GetAllEdProgramsQuery request, CancellationToken cancellationToken)
        {
            return await context.EducationalPrograms
                .AsNoTracking()
                .Skip(request.Offset)
                .Take(request.Limit)
                .ProjectTo<EducationalProgramSummaryResponseDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
