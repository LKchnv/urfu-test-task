﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Extensions;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.DbModels.Enums;
using UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos;
using UrfuTestTask.Core.Models.Dtos.InstituteDtos;
using UrfuTestTask.Core.Models.Dtos.ProgramHeadDtos;

namespace UrfuTestTask.UseCases.EducationalPrograms.Queries
{
    public class GetEdProgramsReferencesQuery : IRequest<EducationalProgramReferencesDto>
    { }

    public class GetEdProgramsReferencesQueryHandler
        : AbstractHandler, IRequestHandler<GetEdProgramsReferencesQuery, EducationalProgramReferencesDto>
    {
        public GetEdProgramsReferencesQueryHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task<EducationalProgramReferencesDto> Handle(GetEdProgramsReferencesQuery request, CancellationToken cancellationToken)
        {
            var institutes = await context
                .Institutes
                .Select(i => new InstituteDto
                {
                    Id = i.Uuid,
                    Title = i.Title
                })
                .ToListAsync();

            var heads = await context
                .ProgramHeads
                .Select(i => new ProgramHeadDto
                {
                    Id = i.Uuid,
                    Fullname = i.Fullname
                })
                .ToListAsync();

            return new EducationalProgramReferencesDto
            {
                Institutes = institutes,
                ProgramHeads = heads,
                EducationLevels = EnumExtensions.GetAllDescriptions<EducationLevelEnum>(),
                EducationStandarts = EnumExtensions.GetAllDescriptions<EducationStandartEnum>(),
            };
        }
    }
}
