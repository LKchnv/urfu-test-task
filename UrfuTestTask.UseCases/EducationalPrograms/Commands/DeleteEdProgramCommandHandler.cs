﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;

namespace UrfuTestTask.UseCases.EducationalPrograms.Commands
{
    public class DeleteEdProgramCommand : IRequest
    {
        public Guid Uuid { get; set; }
    }

    public class DeleteEdProgramCommandHandler
        : AbstractHandler, IRequestHandler<DeleteEdProgramCommand>
    {
        public DeleteEdProgramCommandHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task Handle(DeleteEdProgramCommand request, CancellationToken cancellationToken)
        {
            var toDelete = await context
                .EducationalPrograms
                .Include(e => e.ModulesByEducationalProgram)
                .SingleOrDefaultAsync(e => e.Uuid == request.Uuid);

            if(toDelete == null)
            {
                throw new NotFoundException($"Образовательной програмы с uuid {request.Uuid} не существует");
            }
                
            context.ModulesByEducationalPrograms
                .RemoveRange(toDelete.ModulesByEducationalProgram);

            context.EducationalPrograms.Remove(toDelete);

            await context.SaveChangesAsync();
        }
    }
}
