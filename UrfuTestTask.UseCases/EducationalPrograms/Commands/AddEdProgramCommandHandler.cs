﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.DbModels;
using UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos;

namespace UrfuTestTask.UseCases.EducationalPrograms.Commands
{
    public class AddEdProgramCommand : IRequest
    {
        public EducationalProgramRequestDto Program { get; set; }
    }

    public class AddEdProgramCommandHandler
        : AbstractHandler, IRequestHandler<AddEdProgramCommand>
    {
        public AddEdProgramCommandHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task Handle(AddEdProgramCommand request, CancellationToken cancellationToken)
        {
            var headExists = await context.ProgramHeads.AnyAsync(h => h.Uuid == request.Program.Head);
            if (!headExists)
            {
                throw new NotFoundException($"Руководителя с uuid {request.Program.Head} не существует.");
            }

            var instituteExists = await context.Institutes.AnyAsync(i => i.Uuid == request.Program.Institute);
            if (!instituteExists)
            {
                throw new NotFoundException($"Института с uuid {request.Program.Institute} не существует.");
            }

            if (!ValidateModulesExistance(request.Program.Modules))
            {
                throw new NotFoundException($"Указаны несуществующие модули.");
            }

            var toAdd = mapper.Map<EducationalProgram>(request.Program);
            toAdd.Uuid = Guid.NewGuid();
            await context.EducationalPrograms.AddAsync(toAdd, cancellationToken);
            await context.SaveChangesAsync(cancellationToken);

            foreach (var moudleGuid in request.Program.Modules)
            {
                var moduleToAdd = new ModulesByEducationalProgram
                {
                    Id = Guid.NewGuid(),
                    EducationalProgramUuid = toAdd.Uuid,
                    ModuleUuid = moudleGuid
                };
                await context.ModulesByEducationalPrograms.AddAsync(moduleToAdd);
            }

            await context.SaveChangesAsync(cancellationToken);
        }

        private bool ValidateModulesExistance(IEnumerable<Guid> patchGuids)
        {
            var foundModules = context
                .Modules
                .AsNoTracking()
                .Where(m => patchGuids.Contains(m.Uuid))
                .AsEnumerable();

            var foundModulesGuids = foundModules.Select(m => m.Uuid);

            return foundModulesGuids.SequenceEqual(patchGuids);
        }
    }
}
