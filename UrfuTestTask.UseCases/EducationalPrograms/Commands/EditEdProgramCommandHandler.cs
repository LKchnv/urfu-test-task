﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.DbModels;
using UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos;

namespace UrfuTestTask.UseCases.EducationalPrograms.Commands
{
    public class EditEdProgramCommand : IRequest
    {
        public Guid Uuid { get; set; }
        public JsonPatchDocument<EducationalProgramRequestDto> PatchModel { get; set; }
    }

    public class EditEdProgramCommandHandler
        : AbstractHandler, IRequestHandler<EditEdProgramCommand>
    {
        public EditEdProgramCommandHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task Handle(EditEdProgramCommand request, CancellationToken cancellationToken)
        {
            var existingEntity = await context
                .EducationalPrograms
                .Include(e => e.ModulesByEducationalProgram)
                .FirstOrDefaultAsync(ep => ep.Uuid == request.Uuid);

            if (existingEntity == null)
            {
                throw new NotFoundException($"Образовательной програмы с uuid {request.Uuid} не существует");
            }

            var patchModules = GetPatchModules(request.PatchModel);
            if (patchModules != null && !ValidateModulesExistance(patchModules))
            {
                throw new NotFoundException($"Один или несколько указанных модулей не существует");
            }

            ApplyGeneralChanges(existingEntity, request.PatchModel);
            if(patchModules != null)
                ApplyModules(existingEntity.Uuid, patchModules);

            await context.SaveChangesAsync();
        }

        private void ApplyGeneralChanges
            (EducationalProgram? existingEntity, JsonPatchDocument<EducationalProgramRequestDto> patchModel)
        {
            var existingEntityDto = mapper.Map<EducationalProgramRequestDto>(existingEntity);
            patchModel.ApplyTo(existingEntityDto);
            mapper.Map(existingEntityDto, existingEntity);
        }

        private void ApplyModules
            (Guid educationProgramUuuid, IEnumerable<Guid>? patchModules)
        {
            var toRemove = context.ModulesByEducationalPrograms
                .Where(m => m.EducationalProgramUuid == educationProgramUuuid);

            context.ModulesByEducationalPrograms
                .RemoveRange(toRemove);

            var newModules = patchModules
                .Select(m => new ModulesByEducationalProgram
                {
                    Id = Guid.NewGuid(),
                    EducationalProgramUuid = educationProgramUuuid,
                    ModuleUuid = m
                });

            context.ModulesByEducationalPrograms
                .AddRange(newModules);
        }

        private IEnumerable<Guid>? GetPatchModules(JsonPatchDocument<EducationalProgramRequestDto> patchDocument)
        {
            var desiredState = new EducationalProgramRequestDto();
            patchDocument.ApplyTo(desiredState);
            return desiredState.Modules;
        }

        private bool ValidateModulesExistance(IEnumerable<Guid> patchGuids)
        {
            var foundModules = context
                .Modules
                .Where(m => patchGuids.Contains(m.Uuid))
                .AsEnumerable();

            var foundModulesGuids = foundModules.Select(m => m.Uuid);

            return foundModulesGuids.SequenceEqual(patchGuids) && foundModulesGuids.Any();
        }
    }
}
