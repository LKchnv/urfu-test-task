﻿using AutoMapper;
using UrfuTestTask.Core.Interfaces;

namespace UrfuTestTask.UseCases
{
    public abstract class AbstractHandler
    {
        protected readonly IUrfuProgramsContext context;
        protected readonly IMapper mapper;

        public AbstractHandler(IUrfuProgramsContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
    }
}
