﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;

namespace UrfuTestTask.UseCases.Modules.Commands
{
    public class DeleteModuleCommand : IRequest
    {
        public Guid Uuid { get; set; }
    }

    public class DeleteModuleCommandHandler
        : AbstractHandler, IRequestHandler<DeleteModuleCommand>
    {
        public DeleteModuleCommandHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task Handle(DeleteModuleCommand request, CancellationToken cancellationToken)
        {
            var toDelete = await context
                .Modules
                .Include(m => m.ModulesByEducationalProgram)
                .SingleOrDefaultAsync(e => e.Uuid == request.Uuid);

            if (toDelete == null)
            {
                throw new NotFoundException($"Модуля с uuid {request.Uuid} не существует");
            }

            context.ModulesByEducationalPrograms
                .RemoveRange(toDelete.ModulesByEducationalProgram);

            context.Modules.Remove(toDelete);

            await context.SaveChangesAsync();
        }
    }
}
