﻿using AutoMapper;
using MediatR;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.DbModels;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;

namespace UrfuTestTask.UseCases.Modules.Commands
{
    public class AddModuleCommand : IRequest
    {
        public ModuleRequestDto ModuleToAdd { get; set; }
    }

    public class AddModuleCommandHandler
        : AbstractHandler, IRequestHandler<AddModuleCommand>
    {
        public AddModuleCommandHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task Handle(AddModuleCommand request, CancellationToken cancellationToken)
        {
            var toAdd = mapper.Map<Module>(request.ModuleToAdd);
            toAdd.Uuid = Guid.NewGuid();

            await context.Modules.AddAsync(toAdd);
            await context.SaveChangesAsync();
        }
    }
}
