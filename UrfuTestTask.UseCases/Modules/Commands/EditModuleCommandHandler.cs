﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;

namespace UrfuTestTask.UseCases.Modules.Commands
{
    public class EditModuleCommand : IRequest
    {
        public Guid Uuid { get; set; }
        public JsonPatchDocument<ModuleRequestDto> PatchModel { get; set; }
    }

    public class EditModuleCommandHandler
        : AbstractHandler, IRequestHandler<EditModuleCommand>
    {
        public EditModuleCommandHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task Handle(EditModuleCommand request, CancellationToken cancellationToken)
        {
            var existingEntity = await context
                .Modules
                .FirstOrDefaultAsync(m => m.Uuid == request.Uuid);

            if (existingEntity == null)
            {
                throw new NotFoundException($"Модля с uuid {request.Uuid} не существует");
            }

            var existingEntityDto = mapper.Map<ModuleRequestDto>(existingEntity);
            request.PatchModel.ApplyTo(existingEntityDto);
            mapper.Map(existingEntityDto, existingEntity);

            await context.SaveChangesAsync();
        }
    }
}
