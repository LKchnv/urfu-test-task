﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Exceptions;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;

namespace UrfuTestTask.UseCases.Modules.Queries
{
    public class GetModuleByIdQuery : IRequest<ModuleResponseDto>
    {
        public Guid Uuid { get; set; }
    }

    public class GetModuleByIdQueryHandler
        : AbstractHandler, IRequestHandler<GetModuleByIdQuery, ModuleResponseDto>
    {
        public GetModuleByIdQueryHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task<ModuleResponseDto> Handle(GetModuleByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await context.Modules
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.Uuid == request.Uuid);

            if (result == null)
            {
                throw new NotFoundException($"Модуля с uuid {request.Uuid} не существует");
            }

            return mapper.Map<ModuleResponseDto>(result);
        }
    }
}
