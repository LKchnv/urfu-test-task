﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using UrfuTestTask.Core.Interfaces;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;

namespace UrfuTestTask.UseCases.Modules.Queries
{
    public class GetAllModulesQuery : IRequest<List<ModuleResponseDto>>
    {
        public int Limit { get; set; }
        public int Offset { get; set; }
    }

    public class GetAllModulesQueryHandler
        : AbstractHandler, IRequestHandler<GetAllModulesQuery, List<ModuleResponseDto>>
    {
        public GetAllModulesQueryHandler(IUrfuProgramsContext context, IMapper mapper) 
            : base(context, mapper)
        { }

        public async Task<List<ModuleResponseDto>> Handle(GetAllModulesQuery request, CancellationToken cancellationToken)
        {
            return await context.Modules
                .AsNoTracking()
                .Skip(request.Offset)
                .Take(request.Limit)
                .ProjectTo<ModuleResponseDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
