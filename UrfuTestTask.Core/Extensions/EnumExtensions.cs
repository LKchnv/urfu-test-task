﻿using System.ComponentModel;
using System.Reflection;

namespace UrfuTestTask.Core.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Any())
                {
                    return attributes.First().Description;
                }
            }

            return value.ToString();
        }

        public static TEnum GetValueFromDescription<TEnum>(string description) where TEnum : struct, Enum
        {
            foreach (var field in typeof(TEnum).GetFields())
            {
                if (Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description == description)
                    {
                        return (TEnum)field.GetValue(null);
                    }
                }
                else
                {
                    if (field.Name == description)
                    {
                        return (TEnum)field.GetValue(null);
                    }
                }
            }

            throw new ArgumentException($"Unable to convert \"{description}\" to enum {typeof(TEnum).Name}");
        }

        public static List<string> GetAllDescriptions<TEnum>() where TEnum : Enum
        {
            var type = typeof(TEnum);
            if (!type.IsEnum) throw new ArgumentException("Type must be an enum");

            var descriptions = new List<string>();
            foreach (var field in type.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                var descriptionAttribute = field.GetCustomAttribute<DescriptionAttribute>();
                if (descriptionAttribute != null)
                {
                    descriptions.Add(descriptionAttribute.Description);
                }
            }
            return descriptions;
        }
    }
}
