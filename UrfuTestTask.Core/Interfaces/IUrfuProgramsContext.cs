﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Diagnostics.CodeAnalysis;
using UrfuTestTask.Core.Models.DbModels;

namespace UrfuTestTask.Core.Interfaces
{
    public interface IUrfuProgramsContext
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        public DbSet<EducationalProgram> EducationalPrograms { get; set; }
        public DbSet<Institute> Institutes { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<ProgramHead> ProgramHeads { get; set; }
        public DbSet<ModulesByEducationalProgram> ModulesByEducationalPrograms { get; set; }
    }
}
