﻿using AutoMapper;
using System;
using UrfuTestTask.Core.Extensions;
using UrfuTestTask.Core.Models.DbModels;
using UrfuTestTask.Core.Models.DbModels.Enums;
using UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;

namespace UrfuTestTask.Core.Models.MappingProfiles
{
    public class EducationalProgramMapperProfile : Profile
    {
        public EducationalProgramMapperProfile()
        {
            CreateMap<EducationalProgramRequestDto, EducationalProgram>()
                .ForMember(dest => dest.Uuid, opt => opt.Ignore())
                .ForMember(dest => dest.ModulesByEducationalProgram, opt => opt.Ignore())
                .ForMember(dest => dest.Level, 
                    opt => opt.MapFrom(src => EnumExtensions.GetValueFromDescription<EducationLevelEnum>(src.Level)))
                .ForMember(dest => dest.Standart,
                    opt => opt.MapFrom(src => EnumExtensions.GetValueFromDescription<EducationStandartEnum>(src.Standart)));

            CreateMap<EducationalProgram, EducationalProgramRequestDto>()
                .ForMember(dest => dest.Level,
                    opt => opt.MapFrom(src => src.Level.GetDescription()))
                .ForMember(dest => dest.Standart,
                    opt => opt.MapFrom(src => src.Standart.GetDescription()))
                .ForMember(dest => dest.Modules,
                    opt => opt.MapFrom(src => src.ModulesByEducationalProgram.Select(m => m.ModuleUuid)));

            CreateMap<EducationalProgram, EducationalProgramResponseDto>()
                .ForMember(dest => dest.Modules,
                    opt => opt.MapFrom(src => src.ModulesByEducationalProgram
                        .Select(m => new ModuleResponseDto { Uuid = m.ModuleUuid, Title = m.Module.Title, Type = m.Module.Type } )))
                .ForMember(dest => dest.Level,
                    opt => opt.MapFrom(src => src.Level.GetDescription()))
                .ForMember(dest => dest.Standart,
                    opt => opt.MapFrom(src => src.Standart.GetDescription()))
                .ForMember(dest => dest.HeadFullname,
                    opt => opt.MapFrom(src => src.HeadNavigation.Fullname))
                .ForMember(dest => dest.InstituteTitle,
                    opt => opt.MapFrom(src => src.InstituteNavigation.Title));

            CreateMap<EducationalProgram, EducationalProgramSummaryResponseDto>()
                .ForMember(dest => dest.Level,
                    opt => opt.MapFrom(src => src.Level.GetDescription()))
                .ForMember(dest => dest.Standart,
                    opt => opt.MapFrom(src => src.Standart.GetDescription()))
                .ForMember(dest => dest.HeadFullname,
                    opt => opt.MapFrom(src => src.HeadNavigation.Fullname))
                .ForMember(dest => dest.InstituteTitle,
                    opt => opt.MapFrom(src => src.InstituteNavigation.Title));
        }
    }
}
