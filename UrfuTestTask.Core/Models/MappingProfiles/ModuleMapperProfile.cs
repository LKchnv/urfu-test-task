﻿using AutoMapper;
using UrfuTestTask.Core.Models.DbModels;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;

namespace UrfuTestTask.Core.Models.MappingProfiles
{
    public class ModuleMapperProfile : Profile
    {
        public ModuleMapperProfile()
        {
            CreateMap<Module, ModuleRequestDto>();
            CreateMap<Module, ModuleResponseDto>();
            CreateMap<ModuleRequestDto, Module>();
        }

    }
}
