﻿using System.Text.Json;

namespace UrfuTestTask.Core.Models.Dtos
{
    public record ExceptionResponseDto
    {
        public string ExceptionMessage { get; init; }
        public int ResultStatus { get; init; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
