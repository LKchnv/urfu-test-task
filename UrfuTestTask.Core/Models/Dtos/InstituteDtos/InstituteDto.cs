﻿namespace UrfuTestTask.Core.Models.Dtos.InstituteDtos
{
    public record InstituteDto
    {
        public Guid Id { get; init; }
        public string Title { get; init; } = null!;
    }
}
