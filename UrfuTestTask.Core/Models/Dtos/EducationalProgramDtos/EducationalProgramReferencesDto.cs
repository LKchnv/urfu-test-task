﻿using UrfuTestTask.Core.Models.Dtos.InstituteDtos;
using UrfuTestTask.Core.Models.Dtos.ProgramHeadDtos;

namespace UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos
{
    public record EducationalProgramReferencesDto
    {
        public List<string> EducationLevels { get; init; } = null!;
        public List<string> EducationStandarts { get; init; } = null!;
        public List<InstituteDto> Institutes { get; init; } = null!;
        public List<ProgramHeadDto> ProgramHeads { get; set; } = null!;
    }
}
