﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrfuTestTask.Core.Models.DbModels.Enums;

namespace UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos
{
        public record EducationalProgramRequestDto
        {
            public string Title { get; init; } = null!;
            public string Status { get; init; } = null!;
            public string Cypher { get; init; } = null!;
            public string Level { get; init; } = null!;
            public string Standart { get; init; } = null!;
            public Guid Institute { get; init; }
            public Guid Head { get; init; }
            public DateOnly AccreditationTime { get; init; }
            public List<Guid> Modules { get; set; }
        }
}
