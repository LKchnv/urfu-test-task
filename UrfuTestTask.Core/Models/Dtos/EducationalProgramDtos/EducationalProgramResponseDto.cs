﻿using UrfuTestTask.Core.Models.DbModels.Enums;
using UrfuTestTask.Core.Models.Dtos.ModuleDtos;

namespace UrfuTestTask.Core.Models.Dtos.EducationalProgramDtos
{
    public record EducationalProgramSummaryResponseDto
    {
        public Guid Uuid { get; init; }
        public string Title { get; init; } = null!;
        public string Status { get; init; } = null!;
        public string Cypher { get; init; } = null!;
        public string Level { get; init; } = null!;
        public string Standart { get; init; } = null!;
        public string InstituteTitle { get; init; } = null!;
        public string HeadFullname { get; init; } = null!;
        public DateOnly AccreditationTime { get; init; }

    }

    public record EducationalProgramResponseDto : EducationalProgramSummaryResponseDto
    {
        public Guid Institute { get; init; }
        public Guid Head { get; init; }
        public List<ModuleResponseDto> Modules { get; set; }
    }
}
