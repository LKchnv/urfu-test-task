﻿namespace UrfuTestTask.Core.Models.Dtos.ProgramHeadDtos
{
    public record ProgramHeadDto
    {
        public Guid Id { get; init; }
        public string Fullname { get; init; } = null!;
    }
}
