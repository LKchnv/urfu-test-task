﻿namespace UrfuTestTask.Core.Models.Dtos.ModuleDtos
{
    public record ModuleRequestDto
    {
        public string Title { get; init; } = null!;
        public string Type { get; init; } = null!;
    }
}
