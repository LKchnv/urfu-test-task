﻿namespace UrfuTestTask.Core.Models.Dtos.ModuleDtos
{
    public record ModuleResponseDto
    {
        public Guid Uuid { get; init; }
        public string Title { get; init; } = null!;
        public string Type { get; init; } = null!;
    }
}
