﻿using NpgsqlTypes;
using System.ComponentModel;

namespace UrfuTestTask.Core.Models.DbModels.Enums
{
    public enum EducationStandartEnum
    {
        [PgName("Suos")]
        [Description("СУОС")]
        Suos = 0,

        [PgName("FgosVo")]
        [Description("ФГОС ВО")]
        FgosVo = 1,

        [PgName("Sut")]
        [Description("СУТ")]
        Sut = 2,

        [PgName("FgosVpo")]
        [Description("ФГОС ВПО")]
        FgosVpo = 4,

        [PgName("FgosThreePlusPlus")]
        [Description("ФГОС 3++")]
        FgosThreePlusPlus = 8
    }
}
