﻿using NpgsqlTypes;
using System.ComponentModel;

namespace UrfuTestTask.Core.Models.DbModels.Enums
{
    public enum EducationLevelEnum
    {
        [PgName("Bachelor")]
        [Description("Бакалавр")]
        Bachelor = 0,

        [PgName("AppliedBachelor")]
        [Description("Прикладной бакалавриат")]
        AppliedBachelor = 1,

        [PgName("Specialist")]
        [Description("Специалист")]
        Specialist = 2,

        [PgName("Master")]
        [Description("Магистр")]
        Master = 4,

        [PgName("Aspirant")]
        [Description("Аспирант")]
        Aspirant = 8
    }
}
