﻿namespace UrfuTestTask.Core.Models.DbModels
{
    public class ModulesByEducationalProgram
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid EducationalProgramUuid { get; set; }
        public Guid ModuleUuid { get; set; }

        public virtual EducationalProgram EducationalProgram { get; set; }
        public virtual Module Module { get; set; }
    }
}
