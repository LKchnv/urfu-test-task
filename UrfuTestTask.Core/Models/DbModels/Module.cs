﻿using System;
using System.Collections.Generic;

namespace UrfuTestTask.Core.Models.DbModels
{
    public partial class Module
    {
        public Module()
        {
            ModulesByEducationalProgram = new HashSet<ModulesByEducationalProgram>();
        }

        public Guid Uuid { get; set; }
        public string Title { get; set; } = null!;
        public string Type { get; set; } = null!;

        public virtual ICollection<ModulesByEducationalProgram> ModulesByEducationalProgram { get; set; }
    }
}
