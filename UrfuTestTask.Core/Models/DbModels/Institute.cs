﻿using System;
using System.Collections.Generic;

namespace UrfuTestTask.Core.Models.DbModels
{
    public partial class Institute
    {
        public Institute()
        {
            EducationalPrograms = new HashSet<EducationalProgram>();
        }

        public Guid Uuid { get; set; }
        public string Title { get; set; } = null!;

        public virtual ICollection<EducationalProgram> EducationalPrograms { get; set; }
    }
}
