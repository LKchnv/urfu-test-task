﻿using System;
using System.Collections.Generic;

namespace UrfuTestTask.Core.Models.DbModels
{
    public partial class ProgramHead
    {
        public ProgramHead()
        {
            EducationalPrograms = new HashSet<EducationalProgram>();
        }

        public Guid Uuid { get; set; }
        public string Fullname { get; set; } = null!;

        public virtual ICollection<EducationalProgram> EducationalPrograms { get; set; }
    }
}
