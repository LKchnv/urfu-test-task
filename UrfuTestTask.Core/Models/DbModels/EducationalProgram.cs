﻿using UrfuTestTask.Core.Models.DbModels.Enums;

namespace UrfuTestTask.Core.Models.DbModels
{
    public partial class EducationalProgram
    {
        public EducationalProgram()
        {
           ModulesByEducationalProgram = new HashSet<ModulesByEducationalProgram>();
        }
        public Guid Uuid { get; set; }
        public string Title { get; set; } = null!;
        public string Status { get; set; } = null!;
        public string Cypher { get; set; } = null!;
        public EducationLevelEnum Level { get; set; }
        public EducationStandartEnum Standart { get; set; }
        public Guid Institute { get; set; }
        public Guid Head { get; set; }
        public DateOnly AccreditationTime { get; set; }

        public virtual ProgramHead HeadNavigation { get; set; } = null!;
        public virtual Institute InstituteNavigation { get; set; } = null!;
        public virtual ICollection<ModulesByEducationalProgram> ModulesByEducationalProgram { get; set; }
    }
}
